---
docType: content
title: VS Code Extension
---

# VS Code Extension

To get validation directly in VS Code use the [HTML-Validate extension](https://marketplace.visualstudio.com/items?itemName=html-validate.vscode-html-validate):

    ext install html-validate.vscode-html-validate
