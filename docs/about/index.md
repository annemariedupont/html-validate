---
docType: content
title: About html-validate
---

# About

HTML-validate was created by David Sveningsson in early 2016 with a few goals in mind:

- Enterprise and privacy friendly: no data should leave the machine.
- Pluggable and extendable: must be easy to extend with own domain-specific
  functionality and rules.
- Strict and non-forgiving: should never try to autocorrect or guess anything.
- First-class support for views, components and templates, including when using
  javascript frameworks.

## Contact

If you found a bug or want to suggest a new feature/enhancement please [file a
new issue](https://gitlab.com/html-validate/html-validate/issues/new).

For questions about the project feel free to contact the project owner at
[contact@html-validate.org](mailto:contact@html-validate.org).
